﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMove : MonoBehaviour
{
    [Header("Zoom settings")]
    [Tooltip("The distance it becames impossible to zoom in")]
    public float maxZoom = -10f;
    [Tooltip("The distance it becames impossible to zoom out")]
    public float minZoom = -100f;
    public float zoomingSpeed = 10f;

    [Header("Movement settings")]
    [Tooltip("The speed of the movement in unit/sec")]
    public float moveSpeed = 3f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void FixedUpdate()
    {
        Zoom();
        Move();
    }

    private void Zoom()
    {
        Vector3 zoom = new Vector3(0, 0, Discretize(Input.GetAxis("Mouse ScrollWheel")) * zoomingSpeed * Time.deltaTime);
        Vector3 newCameraPos = zoom + transform.position;
        if (newCameraPos.z <= maxZoom & newCameraPos.z >= minZoom)
        {
            transform.position = newCameraPos;
        }
    }

    private void Move()
    {
        float deltaX = Discretize(Input.GetAxisRaw("Horizontal")) * moveSpeed * Time.deltaTime;
        float deltaY = Discretize(Input.GetAxisRaw("Vertical")) * moveSpeed * Time.deltaTime;
        transform.position += new Vector3(deltaX, deltaY, 0);
    }

    /* Take a value between -1 and 1 and discretize it by returning -1 if the value is negative,
     * 1 if the value is positive, 0 if the value is 0.
     */
    private float Discretize(float value)
    {
        if (value == 0f)
            return value;
        else if (value < 0f)
            return -1f;
        else
         return 1f;
    }
}
