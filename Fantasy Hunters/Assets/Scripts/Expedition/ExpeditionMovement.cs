﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExpeditionMovement : MonoBehaviour
{
    [Tooltip("The move speed in Unity unit/second")]
    public float moveSpeed = 4;
    [Tooltip("The rotation speed in degrees/second")]
    public float rotationSpeed = 0.4f;

    Vector3 destination;
    int floorMask;
    float camRayLength = 1000f;


    void Awake()
    {
        destination = GetComponent<Transform>().position;
        floorMask = LayerMask.GetMask("Floor");
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void FixedUpdate()
    {
        FindDestination();
        Move();
        Rotate();
    }

    void Move()
    {
        Vector3 newPosition = Vector3.MoveTowards(transform.position, destination, moveSpeed * Time.deltaTime);
        transform.position = newPosition;   
    }

    void Rotate()
    {
        Vector3 destinationDir = destination - transform.position;
        float angle = Vector3.SignedAngle(transform.right, destinationDir, Vector3.forward);
        if(Mathf.Abs(angle) > rotationSpeed * Time.deltaTime )
        {
            transform.RotateAround(transform.position, Vector3.forward, Mathf.Sign(angle) * rotationSpeed * Time.deltaTime);
        }
        else
        {
            transform.RotateAround(transform.position, Vector3.forward, angle);
        }

    }

    void FindDestination()
    {
        if (Input.GetButton("Fire1"))
        {
            // Transform screen coord to world coord
            Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit floorHit;
            if (Physics.Raycast(camRay, out floorHit, camRayLength, floorMask))
            {
                destination = floorHit.point;
            }
        }
    }
}
